node default {
    include cron-puppet
}

node /^sql\-(staging|production)\-1\./ {
    include cron-puppet

    class {
        'mariadbrepo': version => '10.1'
    }
    include mariadb
}