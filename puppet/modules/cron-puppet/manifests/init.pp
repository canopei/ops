class cron-puppet {
    file { 'post-hook':
        ensure  => file,
        path    => '/usr/local/sng-ops/.git/hooks/post-merge',
        source  => 'puppet:///modules/cron-puppet/post-merge',
        mode    => 0755,
        owner   => root,
        group   => root,
    }
    cron { 'puppet-apply':
        ensure  => present,
        command => "cd /usr/local/sng-ops ; /usr/bin/git pull",
        user    => root,
        minute  => '*/5',
        require => File['post-hook'],
    }
    cron { 'mysql-backup':
        ensure  => present,
        command => "/root/mysql_backup.sh >> /var/log/mysql_backup.log 2>&1",
        user    => root,
        minute  => 0,
        hour    => 1,
    }
}