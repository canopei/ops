class mariadb {
    package {
        'MariaDB-server': ensure => installed
    }

    service { 'mysql':
        ensure => running,
        enable => true,
        subscribe => File['/etc/mysql/my.cnf'],
    }

    file { 'my-cnf':
        ensure  => file,
        path    => '/etc/mysql/my.cnf',
        source  => 'puppet:///modules/mariadb/my.cnf',
        mode    => 0644,
        owner   => root,
        group   => root,
    }
}