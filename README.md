# SNG OPS

Create a seed job using this .groovy file. It will generate a folder, some views and all the needed jobs for building and deploying the Omnes API and application.


### Jenkins server requirements:

- JDK 1.8
- Android SDK
- Node.js, with global:
    - yarn
    - ionic
    - cordova
    - bower
    - gulp


### Required Jenkins plugins:

- ant (with ant-contrib)
- build-pipeline-plugin
- copyartifact
- gradle
- job-dsl
- parameterized-trigger
- publish-over-ssh (with 'stage', 'omnes-cron-cache' and 'omnes-cron-sync' servers configured)
- config-file-provider
- credentials-binding


### Credentials needed:

- none yet