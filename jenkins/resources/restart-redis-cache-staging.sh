#!/bin/sh --
REDIS_POD=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}' --namespace=sng-staging | grep 'sng-redis-cache-' | head -n 1)
if [ -z "$REDIS_POD" ]; then
    echo "Could not find a running 'sng-redis-cache' pod."
    exit 1
fi

kubectl delete pod $REDIS_POD --namespace=sng-staging