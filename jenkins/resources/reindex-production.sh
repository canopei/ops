#!/bin/sh --
SITE_POD=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}' --namespace=sng-production | grep 'sng-site-' | head -n 1)
if [ -z "$SITE_POD" ]; then
    echo "Could not find a running 'sng-site' pod."
    exit 1
fi

kubectl exec -it $SITE_POD --namespace=sng-production -- /bin/sh -c 'cd /var/sng/bin; ./runreindex'