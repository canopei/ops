def jobsFolder = 'sng'
def buildsToKeep = 5
def releasesToKeep = 3
def devUsers = []

def buildConfigFiles = [
    'sng-api-staging.properties',
    'sng-api-production.properties'
]

configFiles {
    buildConfigFiles.each { configFileName ->
        customConfig {
            id(configFileName)
            name(configFileName)
            content(readFileFromWorkspace('jenkins/config_files/' + configFileName))
            comment('SNG build config file.')
            providerId('org.jenkinsci.plugins.configfiles.custom.CustomConfig')
        }
    }
}

folder(jobsFolder) {
    displayName('SNG')
    authorization {
        devUsers.each { devName ->
            permission('hudson.model.Item.Discover', devName)
            permission('hudson.model.Item.Read', devName)
        }
    }
}

def jobNameE2eProductionTest = 'sng-api-e2e-production-test'
def jobNameE2eStagingTest = 'sng-api-e2e-staging-test'

['gatekeeper', 'account', 'auth', 'class', 'sale', 'site', 'staff'].each { component ->
    def jobNamePrepare = 'sng-api-' + component + '-prepare'
    def jobNameTest = 'sng-api-' + component + '-test'
    def jobNameBuild = 'sng-api-' + component + '-build'
    def jobNameStagingDeploy = 'sng-api-' + component + '-staging-deploy'
    def jobNameProductionDeploy = 'sng-api-' + component + '-production-deploy'

    def jobName = component.capitalize()
    buildPipelineView(jobsFolder + '/' + jobName) {
        title(jobName)
        displayedBuilds(buildsToKeep)
        selectedJob(jobNamePrepare)
        alwaysAllowManualTrigger()
        showPipelineParameters()
        refreshFrequency(3)
    }

    def defaultGitRef = 'master'

    // Prepare job
    job(jobsFolder + '/' + jobNamePrepare) {
        authorization {
            devUsers.each { devName ->
                permission('hudson.model.Item.Build', devName)
                permission('hudson.model.Item.Cancel', devName)
                permission('hudson.model.Item.Discover', devName)
                permission('hudson.model.Item.Read', devName)
                permission('hudson.model.Item.Workspace', devName)
                permission('hudson.model.Run.Replay', devName)
            }
        }

        logRotator(-1, buildsToKeep, -1, buildsToKeep)
        concurrentBuild()

        parameters {
            stringParam('GIT_REF', defaultGitRef, 'The Git ref to build and deploy.')
        }

        scm {
          git {
            remote {
              name('origin')
              url('git@bitbucket.org:canopei/' + component + '.git')
              credentials('9b3b9765-ffff-4af5-bc90-7cc7b9ddfabd')
            }

            branches('origin/${GIT_REF}', 'origin/master')

            extensions {
              cleanBeforeCheckout()
            }
          }
        }
        configure { node ->
            def cnode = node / 'scm' / 'extensions' / 'hudson.plugins.git.extensions.impl.PerBuildTag'
            (node / 'scm' / 'extensions').remove cnode

            node / 'properties' / 'hudson.plugins.copyartifact.CopyArtifactPermissionProperty' {
                'projectNameList' {
                    'string'(jobNameTest)
                }
            }
        }
        triggers {
            scm 'H/2 * * * *'
        }
        wrappers {
            timestamps()
            preBuildCleanup()

            credentialsBinding {
                string('DB_SNG_PASS', 'sng-staging-db-password')
                string('DB_SNG_PRODUCTION_PASS', 'sng-production-db-password')
                usernamePassword('MB_SOURCE_NAME', 'MB_SOURCE_PASS', 'sng-staging-mb-source-creds')
            }
        }
        steps {
            configFileBuildStep {
            	managedFiles {
                    configFile {
                        fileId('sng-api-staging.properties')
                        targetLocation('sng-api-staging.properties')
                        replaceTokens(true)
                    }
                    configFile {
                        fileId('sng-api-production.properties')
                        targetLocation('sng-api-production.properties')
                        replaceTokens(true)
                    }
                }
	        }
            ant {
                target('prepare')
                antInstallation('Ant')
                prop('gitCommit', '$GIT_COMMIT')
            }
        }
        publishers {
            archiveArtifacts {
                pattern 'build/prepared.tar.gz'
                onlyIfSuccessful()
            }
            downstream(jobsFolder + '/' + jobNameTest, 'SUCCESS')

            mailer('aris@canopei.com', true, true)
        }
    }

    // Test job
    job(jobsFolder + '/' + jobNameTest) {
        authorization {
            devUsers.each { devName ->
                permission('hudson.model.Item.Build', devName)
                permission('hudson.model.Item.Cancel', devName)
                permission('hudson.model.Item.Discover', devName)
                permission('hudson.model.Item.Read', devName)
                permission('hudson.model.Item.Workspace', devName)
                permission('hudson.model.Run.Replay', devName)
            }
        }

        logRotator(-1, buildsToKeep, -1, buildsToKeep)
        concurrentBuild()

        configure { node ->
            node / 'properties' / 'hudson.plugins.copyartifact.CopyArtifactPermissionProperty' {
                'projectNameList' {
                    'string'(jobNameBuild)
                }
            }
        }
        wrappers {
            timestamps()
            preBuildCleanup()
        }
        steps {
            copyArtifacts(jobNamePrepare) {
                buildSelector {
                    upstreamBuild(true)
                    flatten()
                }
            }
            shell('tar -xzf prepared.tar.gz && rm prepared.tar.gz')
            ant {
                target('test')
                antInstallation('Ant')
            }
        }
        publishers {
            archiveArtifacts {
                pattern 'build/tested.tar.gz'
                onlyIfSuccessful()
            }

            downstream(jobsFolder + '/' + jobNameBuild, 'SUCCESS')
            mailer('aris@canopei.com', true, true)
        }
    }

    // Build job
    job(jobsFolder + '/' + jobNameBuild) {
        authorization {
            devUsers.each { devName ->
                permission('hudson.model.Item.Build', devName)
                permission('hudson.model.Item.Cancel', devName)
                permission('hudson.model.Item.Discover', devName)
                permission('hudson.model.Item.Read', devName)
                permission('hudson.model.Item.Workspace', devName)
                permission('hudson.model.Run.Replay', devName)
            }
        }

        logRotator(-1, buildsToKeep, -1, buildsToKeep)
        concurrentBuild()

        configure { node ->
            node / 'properties' / 'hudson.plugins.copyartifact.CopyArtifactPermissionProperty' {
                'projectNameList' {
                    'string'(jobNameStagingDeploy + ',' + jobNameProductionDeploy)
                }
            }
        }
        wrappers {
            timestamps()
            preBuildCleanup()
        }
        steps {
            copyArtifacts(jobNameTest) {
                buildSelector {
                    upstreamBuild(true)
                    flatten()
                }
            }
            shell('tar -xzf tested.tar.gz && rm tested.tar.gz')
            ant {
                target('build')
                antInstallation('Ant')
            }
            shell('APP_VERSION=`cat VERSION` && echo "IMAGE_VERSION=${APP_VERSION}" > build.properties')
        }
        publishers {
            archiveArtifacts {
                pattern 'build.xml'
                onlyIfSuccessful()
            }

            downstreamParameterized {
                trigger(jobsFolder + '/' + jobNameStagingDeploy) {
                    condition('SUCCESS')
                    parameters {
                        propertiesFile('build.properties', true)
                    }
                }
            }

            buildPipelineTrigger(jobsFolder + '/' + jobNameProductionDeploy) {
                parameters {
                    propertiesFile('build.properties', true)
                }
            }

            mailer('aris@canopei.com', true, true)
        }
    }

    // Deploy jobs
    job(jobsFolder + '/' + jobNameStagingDeploy) {
        authorization {
            devUsers.each { devName ->
                permission('hudson.model.Item.Build', devName)
                permission('hudson.model.Item.Cancel', devName)
                permission('hudson.model.Item.Discover', devName)
                permission('hudson.model.Item.Read', devName)
                permission('hudson.model.Item.Workspace', devName)
                permission('hudson.model.Run.Replay', devName)
            }
        }

        logRotator(-1, buildsToKeep, -1, buildsToKeep)
        concurrentBuild()

        parameters {
            stringParam('IMAGE_VERSION', 'latest')
        }

        wrappers {
            timestamps()
            preBuildCleanup()
        }

        steps {
            copyArtifacts(jobNameBuild) {
                buildSelector {
                    upstreamBuild(true)
                    flatten()
                }
            }
            ant {
                target('deploy-staging')
                antInstallation('Ant')
                prop('imageVersion', '$IMAGE_VERSION')
            }
        }
        publishers {
            buildPipelineTrigger(jobsFolder + '/' + jobNameE2eStagingTest)

            mailer('aris@canopei.com', true, true)
        }
    }

    job(jobsFolder + '/' + jobNameProductionDeploy) {
        authorization {
            devUsers.each { devName ->
                permission('hudson.model.Item.Build', devName)
                permission('hudson.model.Item.Cancel', devName)
                permission('hudson.model.Item.Discover', devName)
                permission('hudson.model.Item.Read', devName)
                permission('hudson.model.Item.Workspace', devName)
                permission('hudson.model.Run.Replay', devName)
            }
        }

        logRotator(-1, buildsToKeep, -1, buildsToKeep)
        concurrentBuild()

        parameters {
            stringParam('IMAGE_VERSION', 'latest')
        }

        wrappers {
            timestamps()
            preBuildCleanup()
        }

        steps {
            copyArtifacts(jobNameBuild) {
                buildSelector {
                    upstreamBuild(true)
                    flatten()
                }
            }

            ant {
                target('deploy-production')
                antInstallation('Ant')
                prop('imageVersion', '$IMAGE_VERSION')
            }
        }
        publishers {
            buildPipelineTrigger(jobsFolder + '/' + jobNameE2eProductionTest)

            mailer('aris@canopei.com', true, true)
        }
    }
}

job(jobsFolder + '/' + jobNameE2eStagingTest) {
    authorization {
        devUsers.each { devName ->
            permission('hudson.model.Item.Build', devName)
            permission('hudson.model.Item.Cancel', devName)
            permission('hudson.model.Item.Discover', devName)
            permission('hudson.model.Item.Read', devName)
            permission('hudson.model.Item.Workspace', devName)
            permission('hudson.model.Run.Replay', devName)
        }
    }

    logRotator(-1, buildsToKeep, -1, buildsToKeep)
    concurrentBuild()

    wrappers {
        timestamps()
        preBuildCleanup()
    }

    steps {
    }
    publishers {
        mailer('aris@canopei.com', true, true)
    }
}

job(jobsFolder + '/' + jobNameE2eProductionTest) {
    authorization {
        devUsers.each { devName ->
            permission('hudson.model.Item.Build', devName)
            permission('hudson.model.Item.Cancel', devName)
            permission('hudson.model.Item.Discover', devName)
            permission('hudson.model.Item.Read', devName)
            permission('hudson.model.Item.Workspace', devName)
            permission('hudson.model.Run.Replay', devName)
        }
    }

    logRotator(-1, buildsToKeep, -1, buildsToKeep)
    concurrentBuild()

    wrappers {
        timestamps()
        preBuildCleanup()
    }

    steps {
    }
    publishers {
        mailer('aris@canopei.com', true, true)
    }
}

job(jobsFolder + '/sng-reindex-staging') {
    authorization {
        devUsers.each { devName ->
            permission('hudson.model.Item.Build', devName)
            permission('hudson.model.Item.Cancel', devName)
            permission('hudson.model.Item.Discover', devName)
            permission('hudson.model.Item.Read', devName)
            permission('hudson.model.Item.Workspace', devName)
            permission('hudson.model.Run.Replay', devName)
        }
    }

    logRotator(-1, buildsToKeep, -1, buildsToKeep)

    wrappers {
        timestamps()
        preBuildCleanup()
        timeout {
            absolute(10)
            failBuild()
        }
    }

    steps {
        shell(readFileFromWorkspace('jenkins/resources/reindex-staging.sh'))
    }

    publishers {
        mailer('aris@canopei.com', true, true)
    }
}

job(jobsFolder + '/sng-reindex-production') {
    authorization {
        devUsers.each { devName ->
            permission('hudson.model.Item.Build', devName)
            permission('hudson.model.Item.Cancel', devName)
            permission('hudson.model.Item.Discover', devName)
            permission('hudson.model.Item.Read', devName)
            permission('hudson.model.Item.Workspace', devName)
            permission('hudson.model.Run.Replay', devName)
        }
    }

    logRotator(-1, buildsToKeep, -1, buildsToKeep)

    wrappers {
        timestamps()
        preBuildCleanup()
        timeout {
            absolute(10)
            failBuild()
        }
    }

    steps {
        shell(readFileFromWorkspace('jenkins/resources/reindex-production.sh'))
    }

    publishers {
        mailer('aris@canopei.com', true, true)
    }
}