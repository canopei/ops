def jobsFolder = 'sng'
def buildsToKeep = 5
def releasesToKeep = 3
def devUsers = []

def buildConfigFiles = [
    'sng-website-staging.properties',
    'sng-website-production.properties'
]

def envs = [
	'production',
	'staging'
]

configFiles {
    buildConfigFiles.each { configFileName ->
        customConfig {
            id(configFileName)
            name(configFileName)
            content(readFileFromWorkspace('jenkins/config_files/' + configFileName))
            comment('SNG build config file.')
            providerId('org.jenkinsci.plugins.configfiles.custom.CustomConfig')
        }
    }
}

folder(jobsFolder) {
    displayName('SNG')
    authorization {
        devUsers.each { devName ->
            permission('hudson.model.Item.Discover', devName)
            permission('hudson.model.Item.Read', devName)
        }
    }
}

['website'].each { component ->
    envs.each { envName ->
        def jobName = component.capitalize() + ' ' + envName.capitalize()
        buildPipelineView(jobsFolder + '/' + jobName) {
            title(jobName)
            displayedBuilds(buildsToKeep)
            selectedJob('sng-' + component + '-' + envName + '-build')
            alwaysAllowManualTrigger()
            showPipelineParameters()
            refreshFrequency(3)
        }
    }
}

envs.each { envName ->
    def jobNameWebsiteBuild = 'sng-website' + '-' + envName + '-build'
    def jobNameWebsiteDeploy = 'sng-website' + '-' + envName + '-deploy'

    def defaultGitRef = 'develop'
    if (envName == 'production' || true) {
    	defaultGitRef = 'master'
    }

    def sfEnv
    switch (envName) {
        case 'production':
            sfEnv = 'prod'
            break
        case 'staging':
            sfEnv = 'staging'
            break
        default:
            sfEnv = 'prod'
            break
    }

    job(jobsFolder + '/' + jobNameWebsiteBuild) {
        authorization {
            devUsers.each { devName ->
                permission('hudson.model.Item.Build', devName)
                permission('hudson.model.Item.Cancel', devName)
                permission('hudson.model.Item.Discover', devName)
                permission('hudson.model.Item.Read', devName)
                permission('hudson.model.Item.Workspace', devName)
                permission('hudson.model.Run.Replay', devName)
            }
        }

        logRotator(-1, buildsToKeep, -1, buildsToKeep)
        concurrentBuild()

        parameters {
            stringParam('GIT_REF', defaultGitRef, 'The Git ref to build and deploy.')
        }

        scm {
          git {
            remote {
              name('origin')
              url('git@bitbucket.org:canopei/website.git')
              credentials('9b3b9765-ffff-4af5-bc90-7cc7b9ddfabd')
            }
            // if (envName == 'staging') {
            //     branches('origin/dev-*')
            // } else {
            //     branches('origin/${GIT_REF}', 'origin/master')
            // }
            branches('origin/${GIT_REF}')

            extensions {
              cleanBeforeCheckout()
            }
          }
        }
        configure { node ->
            def cnode = node / 'scm' / 'extensions' / 'hudson.plugins.git.extensions.impl.PerBuildTag'
            (node / 'scm' / 'extensions').remove cnode

            if (envName == 'staging') {
                node / 'properties' / 'hudson.plugins.copyartifact.CopyArtifactPermissionProperty' {
                    'projectNameList' {
                        'string'(jobNameWebsiteDeploy)
                    }
                }
            } else {
                node / 'properties' / 'hudson.plugins.copyartifact.CopyArtifactPermissionProperty' {
                    'projectNameList' {
                        'string'(jobNameWebsiteDeploy + ',sng-website-production-preview-deploy')
                    }
                }
            }
        }
        triggers {
            scm 'H/2 * * * *'
        }
        wrappers {
            timestamps()
            preBuildCleanup()

            credentialsBinding {
                string('FACEBOOK_APP_SECRET', 'sng-' + envName + '-fb-app-secret')
                string('GOOGLE_APP_SECRET', 'sng-' + envName + '-google-app-secret')
                string('INTERCOM_ACCESS_TOKEN', 'sng-' + envName + '-intercom-access-token')
                string('INTERCOM_SECRET', 'sng-' + envName + '-intercom-secret')
                string('STRIPE_SECRET_API_KEY', 'sng-' + envName + '-stripe-secret-api-key')
            }
        }
        steps {
            configFileBuildStep {
            	managedFiles {
                    configFile {
                        fileId('sng-website-' + envName + '.properties')
                        targetLocation('sng-website.properties')
                        replaceTokens(true)
                    }
                }
	        }
            ant {
                antInstallation('Ant')
                prop('sfEnv', sfEnv)
            }
        }
        publishers {
            archiveArtifacts {
                pattern 'build/build.tar.gz'
                onlyIfSuccessful()
            }

            if (envName == 'staging') {
                buildPipelineTrigger(jobsFolder + '/' + jobNameWebsiteDeploy)
            } else {
                buildPipelineTrigger(jobsFolder + '/' + jobNameWebsiteDeploy + ',' + jobsFolder + '/sng-website-production-preview-deploy')
            }

            mailer('aris@canopei.com', true, true)
        }
    }

    job(jobsFolder + '/clear-redis-cache-' + envName) {
        authorization {
            devUsers.each { devName ->
                permission('hudson.model.Item.Build', devName)
                permission('hudson.model.Item.Cancel', devName)
                permission('hudson.model.Item.Discover', devName)
                permission('hudson.model.Item.Read', devName)
                permission('hudson.model.Item.Workspace', devName)
                permission('hudson.model.Run.Replay', devName)
            }
        }

        wrappers {
            timestamps()
            preBuildCleanup()
        }

        steps {
            shell(readFileFromWorkspace('jenkins/resources/restart-redis-cache-staging.sh'))
        }

        publishers {
            mailer('aris@canopei.com', true, true)
        }
    }
}

job(jobsFolder + '/sng-website-staging-deploy') {
    authorization {
        devUsers.each { devName ->
            permission('hudson.model.Item.Build', devName)
            permission('hudson.model.Item.Cancel', devName)
            permission('hudson.model.Item.Discover', devName)
            permission('hudson.model.Item.Read', devName)
            permission('hudson.model.Item.Workspace', devName)
            permission('hudson.model.Run.Replay', devName)
        }
    }

    logRotator(-1, buildsToKeep, -1, buildsToKeep)
    concurrentBuild()

    wrappers {
        timestamps()
        preBuildCleanup()
    }

    steps {
        copyArtifacts 'sng-website-staging-build', {
            buildSelector {
                upstreamBuild(true)
                flatten()
            }
        }

        publishOverSsh {
        	server('stage') {
                transferSet {
                    sourceFiles('build.tar.gz')
                    remoteDirectory('/var/www/sng/vip1/website/releases/$BUILD_NUMBER')
                    execCommand('cd /var/www/sng/vip1/website/releases/$BUILD_NUMBER && tar -xzf build.tar.gz && rm build.tar.gz && ln -nfs releases/$BUILD_NUMBER ../../current')
                }
                transferSet {
                    execCommand('chmod -R 0777 /var/www/sng/vip1/website/releases/$BUILD_NUMBER/var')
                }
                transferSet {
                    execCommand('mkdir -p /var/www/sng/vip1/website/shared && touch /var/www/sng/vip1/website/shared/documents.txt && chmod 0777 /var/www/sng/vip1/website/shared/documents.txt && ln -nfs /var/www/sng/vip1/website/shared/documents.txt /var/www/sng/vip1/website/current/web/documents.txt');
                }
                transferSet {
                    execCommand('cd /var/www/sng/vip1/website/releases && ls -t | tail -n +' + (releasesToKeep + 1) + ' | xargs rm -r --')
                }
                transferSet {
                    execCommand('sudo systemctl restart nginx')
                }
            }
    	}
    }
    publishers {
        mailer('aris@canopei.com', true, true)
    }
}

job(jobsFolder + '/sng-website-production-deploy') {
    authorization {
        devUsers.each { devName ->
            permission('hudson.model.Item.Build', devName)
            permission('hudson.model.Item.Cancel', devName)
            permission('hudson.model.Item.Discover', devName)
            permission('hudson.model.Item.Read', devName)
            permission('hudson.model.Item.Workspace', devName)
            permission('hudson.model.Run.Replay', devName)
        }
    }

    logRotator(-1, buildsToKeep, -1, buildsToKeep)
    concurrentBuild()

    wrappers {
        timestamps()
        preBuildCleanup()
    }

    steps {
        copyArtifacts 'sng-website-production-build', {
            buildSelector {
                upstreamBuild(true)
                flatten()
            }
        }

        publishOverSsh {
        	server('sng-www-prod-01') {
                transferSet {
                    sourceFiles('build.tar.gz')
                    remoteDirectory('/var/www/sng/prod/website/releases/$BUILD_NUMBER')
                    execCommand('cd /var/www/sng/prod/website/releases/$BUILD_NUMBER && tar -xzf build.tar.gz && rm build.tar.gz && ln -nfs releases/$BUILD_NUMBER ../../current')
                }
                transferSet {
                    execCommand('chmod -R 0777 /var/www/sng/prod/website/releases/$BUILD_NUMBER/var')
                }
                transferSet {
                    execCommand('mkdir -p /var/www/sng/prod/website/shared && touch /var/www/sng/prod/website/shared/documents.txt && chmod 0777 /var/www/sng/prod/website/shared/documents.txt && ln -nfs /var/www/sng/prod/website/shared/documents.txt /var/www/sng/prod/website/current/web/documents.txt');
                }
                transferSet {
                    execCommand('cd /var/www/sng/prod/website/releases && ls -t | tail -n +' + (releasesToKeep + 1) + ' | xargs rm -r --')
                }
                transferSet {
                    execCommand('sudo service nginx reload')
                }
            }
    	}
    }
    publishers {
        mailer('aris@canopei.com', true, true)
    }
}

job(jobsFolder + '/sng-website-production-preview-deploy') {
    authorization {
        devUsers.each { devName ->
            permission('hudson.model.Item.Build', devName)
            permission('hudson.model.Item.Cancel', devName)
            permission('hudson.model.Item.Discover', devName)
            permission('hudson.model.Item.Read', devName)
            permission('hudson.model.Item.Workspace', devName)
            permission('hudson.model.Run.Replay', devName)
        }
    }

    logRotator(-1, buildsToKeep, -1, buildsToKeep)
    concurrentBuild()

    wrappers {
        timestamps()
        preBuildCleanup()
    }

    steps {
        copyArtifacts 'sng-website-production-build', {
            buildSelector {
                upstreamBuild(true)
                flatten()
            }
        }

        publishOverSsh {
        	server('sng-www-prod-01') {
                transferSet {
                    sourceFiles('build.tar.gz')
                    remoteDirectory('/var/www/sng/preview/website/releases/$BUILD_NUMBER')
                    execCommand('cd /var/www/sng/preview/website/releases/$BUILD_NUMBER && tar -xzf build.tar.gz && rm build.tar.gz && ln -nfs releases/$BUILD_NUMBER ../../current')
                }
                transferSet {
                    execCommand('chmod -R 0777 /var/www/sng/preview/website/releases/$BUILD_NUMBER/var')
                }
                transferSet {
                    execCommand('mkdir -p /var/www/sng/preview/website/shared && touch /var/www/sng/preview/website/shared/documents.txt && chmod 0777 /var/www/sng/preview/website/shared/documents.txt && ln -nfs /var/www/sng/preview/website/shared/documents.txt /var/www/sng/preview/website/current/web/documents.txt');
                }
                transferSet {
                    execCommand('cd /var/www/sng/preview/website/releases && ls -t | tail -n +' + (releasesToKeep + 1) + ' | xargs rm -r --')
                }
                transferSet {
                    execCommand('sudo service nginx reload')
                }
            }
    	}
    }
    publishers {
        mailer('aris@canopei.com', true, true)
    }
}